Escreva, neste arquivo, sua resposta para a pergunta da parte 2.2 do MiniEP6. Descreva brevemente o que você observou ao executar mostra_n(20). Não precisa entrar em detalhes! Basta 2 ou 3 linhas.

Nota-se que a sequência de números impares consecutivos para 20 são os 20 primeiros ímpares após o último ímpar da sequência de ímpares consecutivos somados de 19, e por aí vai. Ou seja, se o último ímpar da sequência de 9 é o 89, o primeiro da sequência para 10 é 91.
