# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    i = 1
    while i <= n^3
        soma = 0
        impar_atual = i
        for j = 1:n
            soma = soma + impar_atual
            impar_atual = impar_atual + 2
        end
        
        if (soma == n^3)
            return i
        end

        i = i + 2
    end
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    print(m, " ", m^3)
    impar = impares_consecutivos(m)

    for i = 1:m
        print(" ", impar)
        impar = impar + 2
    end
end

function mostra_n(n)
    for i = 1:n
        imprime_impares_consecutivos(i)
        println()
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end